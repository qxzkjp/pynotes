from sine_wave import exponentialDieoffFactory, NotePlayerContext
import pyaudio

class MorseCoder:
    _codes = {
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..-',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        '0': '-----',
        '.': '.-.-.-',
        ',': '--..--',
        '?': '..--..',
        "'": '.----.',
        '/': '-..-.',
        '(': '-.--.',
        ')': '-.--.-',
        '&': '.-...',
        ':': '---...',
        '=': '-...-',
        '+': '.-.-.',
        '-': '-....-',
        '"': '.-..-.',
        '@': '.--.-.',
        'EOW': '...-.-',
        'ERR': '........',
        'INV': '-.-',
        'START': '-.-.-',
        'PAGE': '.-.-.',
        'ACK': '...-.',
        'WAIT': '.-...'
        }
    def __init__(self, player, frequency = 440, scale = 0.1):
        self._player = player
        self._frequency = frequency
        self._scale = scale
        self._wordEnded = False
        self._letterEnded = False
        self._emptyBuffer = True
    def queueDot(self):
        self._queueSeparator()
        self._player.queueNote(self._frequency, self._scale)
        self._emptyBuffer = False
    def queueDash(self):
        self._queueSeparator()
        self._player.queueNote(self._frequency, self._scale * 3.0)
        self._emptyBuffer = False
    def endLetter(self):
        self._letterEnded = True
    def endWord(self):
        self._wordEnded = True
    def _queueSeparator(self):
        if self._emptyBuffer:
            pass
        elif self._wordEnded:
            self._player.queueNote(0, self._scale * 7.0, 0.0)
        elif self._letterEnded:
            self._player.queueNote(0, self._scale * 3.0, 0.0)
        else:
            self._player.queueNote(0, self._scale, 0.0)
        self._letterEnded = False
        self._wordEnded = False
    def _queueLetter(self, letter):
        coding = MorseCoder._codes[letter.upper()]
        self.queueMorse(coding)
    def queueString(self, string):
        for s in string:
            if s == " ":
                self.endWord()
            else:
                self.queueLetter(s)
        if len(string) > 0:
            self.endWord()
    def queueMorse(self, coding):
        for i in coding:
            if i == ".":
                self.queueDot()
            elif i == "-":
                self.queueDash()
            elif i == " ":
                self.endLetter()
            elif i == "/":
                self.endWord()
    def queueLetter(self, letter):
        self._queueLetter(letter)
        self.endLetter()
    def queueSos(self):
        #SOS is a prosign, so there are no spaces between the letters
        coder._queueLetter("S")
        coder._queueLetter("O")
        coder._queueLetter("S")
        coder.endWord()
    def play(self):
        self._player.playQueuedNotes()
        self._emptyBuffer = True

if __name__ == "__main__":
    with NotePlayerContext(pyaudio.PyAudio()) as player:
        shaper = exponentialDieoffFactory(player.getSampleRate() // 150)
        player.setShaper(shaper)
        coder = MorseCoder(player, 860, 0.06)
        coder.queueSos()
        coder.queueSos()
        coder.queueSos()
        coder.queueLetter("START")
        coder.endWord()
        coder.queueString("TO WHOM IT MAY CONCERN +")
        coder.queueString("WE ARE PRESENTLY TRAPPED ON AN ISLAND +")
        coder.queueString("LOCALS ARE SAVAGE AND THREATENING +")
        coder.queueString("CO ORDINATES 50.67N 1.29W")
        coder.queueLetter("EOW")
        coder.play()
