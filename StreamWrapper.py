import pyaudio

class StreamWrapper:
    def __init__(self, context, sampleRate = 44100, bufSize = 22500):
        self._sampleRate = sampleRate
        self._stream = context.open(
            format=pyaudio.paFloat32,
            channels=1,
            rate=self._sampleRate,
            output=True,
            stream_callback= lambda in_data, frame_count, time_info, status: self.read())
        self._buffer = np.zeros(bufSize)
        self._inPos = 0
        self._outPos = 0
        self._readLock = Lock()
        self._writeLock = Lock()
        self._canWrite = True
        self._stream = stream
    
    def write(self, samples):
        pos = 0
        # do not allow two writes to interleave
        with self._writeLock:
            while pos < len(samples):
                # do not allow other threads to read while we are writing
                with self._readLock:
                    written = self._copyIntoBuffer(samples[pos:])
                    pos = pos + written
    def read(self):
        with self._readLock:
            if self._outPos == self._inPos and self._canWrite:
                return b''

            if self._outPos > self._inPos:
                asBytes = self._buffer[self._inPos:self._outPos].astype(np.float32).tobytes()                
            else:
                asBytes = (self._buffer[self._inPos:].astype(np.float32).tobytes() +
                    self._buffer[:self._outPos].astype(np.float32).tobytes())
            self._outPos = self._outPos + len(asBytes)
            if self._outPos == self._inPos:
                self._canWrite = True
            return asBytes
            
    def _copyIntoBuffer(self, samples):
        if self._inPos == self._outPos and not self.canWrite:
            return 0
        
        # copy into the circular buffer, from the in position
        if self._outPos > self._inPos:
            available = self._outPos - self._inPos
        else:
            available = len(self._buffer) - self._inPos
        
        toCopy = min(available, len(samples))
        np.copyto(self._buffer[self._inPos:self._inPos+toCopy], samples[:toCopy])
        self._inPos = self._inPos + toCopy
        remaining = len(samples) - toCopy
        # if we got all the way to the end and still have data, continue
        # writing from the beginning until we hit the out position
        if remaining > 0 and self._inPos != self._outPos:
            toCopyAgain = min(outPos, remaining)
            np.copyto(self._buffer[:toCopyAgain], samples[toCopy:toCopy+toCopyAgain])
            self._inPos = toCopyAgain
        if self._inPos == self._outPos:
            self._canWrite = False
        return toCopyAgain + toCopy
