import pyaudio
import numpy as np
from threading import Lock
from time import sleep

def decibel(x):
    return 10 ** (x / 20.0)

class _NotePlayer:
    def __init__(self, sampleRate, volume, stream, shaper = None):
        self._sampleRate = int(sampleRate)
        self._volume = volume
        self._stream = stream
        self._buffer = b''
        if shaper is None:
            self._shaper = exponentialDieoffFactory(sampleRate // 150)
        else:
            self._shaper = shaper
    def playNote(self, frequency, duration = 1.0, volume = 1.0):
        samples = self._getSineSamples(frequency, duration, volume)
        self._playSamples(samples)
        
    def _getSineSamples(self, frequency, duration = 1.0, volume = 1.0):
        numSamples = int(self._sampleRate * duration)
        samples = (np.sin(2*np.pi*np.arange(numSamples)*frequency/self._sampleRate))*volume*(self._volume)

        self._shaper(samples)

        # in order to stop the stream mis-interpreting the samples,
        # we convert them to bytes
        samples = samples.astype(np.float32).tobytes()
        return samples
    def _playSamples(self, samples):
        self._stream.write(samples)
    def queueNote(self, frequency, duration = 1.0, volume = 1.0):
        samples = self._getSineSamples(frequency, duration, volume)
        self._buffer = self._buffer + samples
    def playQueuedNotes(self):
        self._playSamples(self._buffer)
        self._buffer = b''
    def getSampleRate(self):
        return self._sampleRate
    def setVolume(self, volume):
        self._volume = volume
    def setShaper(self, shaper):
        self._shaper = shaper

class NotePlayerContext:
    def __init__(self, pyAudio, sampleRate = 44100, volume = 0.5, callback = None):
        self._sampleRate = int(sampleRate)
        self._volume = volume
        self._callback = callback
        self._context = pyAudio
    def __enter__(self):
        self._stream = self._context.open(
            format=pyaudio.paFloat32,
            channels=1,
            rate=self._sampleRate,
            output=True,
            stream_callback=self._callback)
        
        
        return _NotePlayer(self._sampleRate, self._volume, self._stream)
    def __exit__(self, exc_type, exc_value, traceback):
        self._stream.stop_stream()
        self._stream.close()


def linearDieoffFactory(dieLength):
    return lambda samples: multiplyByIterator(
                    samples,
                    linearDieoffIt(len(samples), dieLength)
                )
def exponentialDieoffFactory(dieLength):
    return lambda samples: multiplyByIterator(
                    samples,
                    exponentialDieoffIt(len(samples), dieLength)
                )

def nullShaper(samples):
    pass

def multiplyByIterator(array, iterator):
    for i in range(len(array)):
        array[i] = array[i] * next(iterator)

def linearDieoffIt(samples, fadeLength):
    for i in range(samples):
        if i < fadeLength:
            yield i / fadeLength
        elif samples - i < fadeLength:
            yield (samples - i) / fadeLength
        else:
            yield 1.0

def exponentialDieoffIt(samples, fadeLength):
    for i in range(samples):
        if i < fadeLength:
            yield decibel( -((fadeLength - i) / fadeLength) * 100 )
        elif samples - i < fadeLength:
            yield decibel( -( (i - samples + fadeLength) / fadeLength) * 100 )
        else:
            yield 1.0

# This is excluded from code coverage as it is essentially just a behavioural
# test of the module
if __name__ == "__main__": # pragma: no cover
    with NotePlayerContext(pyaudio.PyAudio()) as player:
        player.setShaper(exponentialDieoffFactory(player.getSampleRate() // 150))
        player.playNote(440)
