import unittest
from unittest.mock import Mock, MagicMock
from math import isclose
import numpy as np
import math
import random
import sine_wave
import notes
import morse

class TestNotePlayer(unittest.TestCase):
    def checkForSineWave(self, samples, period, volume):
        phaseShift = period // 4
        squareSumRoot = np.sqrt( samples[phaseShift:] ** 2 + samples[:len(samples) - phaseShift] ** 2 )
        vol = np.mean(squareSumRoot)
        sd = np.std(squareSumRoot)
        return np.isclose(vol, volume, rtol=0.05) and sd < 0.15

    #this test shifts the waveform by 1/4 of a period, then squares and adds it
    #to itself, to check if it creates a constant line
    def testIsSineWave(self):
        sampleRate = 44100
        #between 0.5 and 1.0
        volume = random.random() * 0.5 + 0.5
        # A_3 - A_5
        frequency = random.randint(220, 880)
        period = int(sampleRate / frequency)
        # between 0.5 and 1.0
        duration = 2 * period
        stream = Mock()
        player = sine_wave._NotePlayer(sampleRate, volume, stream, sine_wave.nullShaper)
        player.playNote(frequency, duration)
        waveform = np.frombuffer(stream.write.call_args[0][0], dtype=np.float32)
        self.assertTrue( self.checkForSineWave(waveform, period, volume ))

    #this test tests that the shaper is actually used
    def testShaperIsCalled(self):
        sampleRate = 44100
        #between 0.5 and 1.0
        volume = random.random() * 0.5 + 0.5
        # A_4
        frequency = 440
        duration = 1.0

        stream = Mock()
        shaper = Mock()
        player = sine_wave._NotePlayer(sampleRate, volume, stream, shaper)
        shaper.assert_not_called()
        player.playNote(frequency, duration)
        shaper.assert_called_once()

    def testSetShaper(self):
        sampleRate = 44100
        #between 0.5 and 1.0
        volume = random.random() * 0.5 + 0.5
        # A_4
        frequency = 440
        duration = 1.0

        stream = Mock()
        shaper = Mock()
        player = sine_wave._NotePlayer(sampleRate, volume, stream, sine_wave.nullShaper)
        player.setShaper(shaper)

        shaper.assert_not_called()
        player.playNote(frequency, duration)
        shaper.assert_called_once()
        

    def testSetVolume(self):
        sampleRate = 44100
        volume = 0.25
        frequency = 440
        duration = 0.1
        
        stream = Mock()
        player = sine_wave._NotePlayer(sampleRate, 1.0, stream, sine_wave.nullShaper)
        player.playNote(frequency, duration, volume)
        data = stream.write.call_args[0][0]
        waveform = np.frombuffer(data, dtype=np.float32)
        
        self.assertTrue(np.isclose(np.max(waveform), volume))
        self.assertTrue(np.isclose(np.min(waveform), -volume))

    def testSetInitialVolume(self):
        sampleRate = 44100
        volume = 0.25
        frequency = 440
        duration = 0.1
        
        stream = Mock()
        player = sine_wave._NotePlayer(sampleRate, volume, stream, sine_wave.nullShaper)
        player.playNote(frequency, duration)
        data = stream.write.call_args[0][0]
        waveform = np.frombuffer(data, dtype=np.float32)
        
        self.assertTrue(np.isclose(np.max(waveform), volume))
        self.assertTrue(np.isclose(np.min(waveform), -volume))

    def testSetGlobalVolume(self):
        sampleRate = 44100
        volume = 0.25
        frequency = 440
        duration = 0.1
        
        stream = Mock()
        player = sine_wave._NotePlayer(sampleRate, 1.0, stream, sine_wave.nullShaper)
        player.setVolume(volume)
        player.playNote(frequency, duration)
        data = stream.write.call_args[0][0]
        waveform = np.frombuffer(data, dtype=np.float32)

        self.assertTrue(np.isclose(np.max(waveform), volume))
        self.assertTrue(np.isclose(np.min(waveform), -volume))
    
    def testSilence(self):
        sampleRate = 44100
        duration = 0.1

        stream = Mock()
        player = sine_wave._NotePlayer(sampleRate, 0.5, stream, sine_wave.nullShaper)
        player.playNote(440, duration, 0.0)
        string = np.frombuffer(stream.write.call_args[0][0], dtype=np.float32)
        self.assertTrue(np.isclose(string, 0).all())

    def testQueueingNotes(self):
        sampleRate = 44100
        duration = 0.1
        
        stream = Mock()
        stream.write = Mock()

        player = sine_wave._NotePlayer(sampleRate, 0.5, stream, sine_wave.nullShaper)
        player.queueNote(440, duration, 0.0)
        stream.write.assert_not_called()
        player.playQueuedNotes()
        stream.write.assert_called_once()

        self.assertTrue(len(stream.write.call_args[0][0]) > 0)
        stream.write.reset_mock()

        player.queueNote(440, duration, 0.0)
        player.queueNote(440, duration, 0.0)
        stream.write.assert_not_called()
        player.playQueuedNotes()
        stream.write.assert_called_once()

    def testGetSampleRate(self):
        sampleRate = random.randint(10000, 100000)
        stream = Mock()
        player = sine_wave._NotePlayer(sampleRate, 0.5, stream, sine_wave.nullShaper)
        self.assertEqual(sampleRate, player.getSampleRate())

class  TestDieoff(unittest.TestCase):
    def testLinearDieoffFactory(self):
        with unittest.mock.patch('sine_wave.linearDieoffIt'):
            dieoffFilter = sine_wave.linearDieoffFactory(100)
            dieoffFilter([])
            sine_wave.linearDieoffIt.assert_called()
            self.assertTrue(callable(dieoffFilter))
        
    def testExponentialDieoffFactory(self):
        with unittest.mock.patch('sine_wave.exponentialDieoffIt'):
            dieoffFilter = sine_wave.exponentialDieoffFactory(100)
            dieoffFilter([])
            sine_wave.exponentialDieoffIt.assert_called()
            self.assertTrue(callable(dieoffFilter))

    def testLinearDieoff(self):
        self.doDieoff(sine_wave.linearDieoffIt)

    def testExponentialDieoff(self):
        self.doDieoff(sine_wave.exponentialDieoffIt)

    def doDieoff(self, func):
        samples = 10000;
        
        max_val = float("-inf")
        min_val = float("inf")
        count = 0
        for i in func(samples, 200):
            if i < min_val:
                min_val = i

            if i > max_val:
                max_val = i
            
            count += 1

        self.assertEqual(samples, count)
        self.assertTrue(min_val >= 0)
        self.assertTrue(max_val <= 1.0)

class TestNoteContext(unittest.TestCase):
    def testStreamCreated(self):
        pya = Mock()
        stream = Mock()
        pya.open = Mock(return_value = stream)
        pya.terminate = Mock()
        with sine_wave.NotePlayerContext(pya) as s:
            pya.open.assert_called_once()

    def testStreamClosed(self):
        pya = Mock()
        stream = Mock()
        stream.stop_stream = Mock()
        stream.close = Mock()
        pya.open = Mock(return_value = stream)
        with sine_wave.NotePlayerContext(pya) as s:
            stream.stop_stream.assert_not_called()
            stream.close.assert_not_called()
        stream.stop_stream.assert_called_once()
        stream.close.assert_called_once()

    def testContextNotTerminated(self):
        pya = Mock()
        stream = Mock()
        pya.open = Mock(return_value = stream)
        pya.terminate = Mock()
        with sine_wave.NotePlayerContext(pya) as s:
            pass
        pya.terminate.assert_not_called()

class TestNotes(unittest.TestCase):
    def testDisplayName(self):
        self.assertEqual("Ab", notes.displayName(0, -1))
        self.assertEqual("C#", notes.displayName(2, 1))
        self.assertEqual("E##", notes.displayName(4, 2))
        self.assertEqual("B", notes.displayName(1, 0))

    def testEnharmonise(self):
        self.assertEqual("C", notes.enharmonise(3, 2))
        self.assertEqual("Gb", notes.enharmonise(9, 6))
        self.assertEqual("E#", notes.enharmonise(8, 4))

    def testBadTagInGetScale(self):
        with self.assertRaises(ValueError) as context:
            notes.getScale(0, 0, "sdljksdfj", 0)

    def testMinorScale(self):
        expected = [
            ((0,0), 311.13),
            ((2,1), 349.23),
            ((3,2), 369.99),
            ((5,3), 415.30),
            ((7,4), 466.16),
            ((8,5), 493.88),
            ((10,6), 554.37),
            ((12,0), 622.25)
            ]
        
        basePos = 3
        baseTone = 6
        with unittest.mock.patch('notes.enharmonise', lambda x,y:(x-baseTone, (y-basePos)%7)):
            result = notes.getScale(basePos, 1, "min", -1)
            self.assertEqual(len(expected), len(result))
            for i in range(len(expected)):
                self.assertEqual(expected[i][0], result[i][0])
                self.assertTrue(math.isclose(expected[i][1], result[i][1], rel_tol=0.05))

    def testMajorScale(self):
        expected = [
            ((0,0), 311.13),
            ((2,1), 349.23),
            ((4,2), 392.00),
            ((5,3), 415.30),
            ((7,4), 466.16),
            ((9,5), 523.25),
            ((11,6), 587.33),
            ((12,0), 622.25)
            ]
        
        basePos = 3
        baseTone = 6
        with unittest.mock.patch('notes.enharmonise', lambda x,y:(x-baseTone, (y-basePos)%7)):
            result = notes.getScale(basePos, 1, "maj", -1)
            self.assertEqual(len(expected), len(result))
            for i in range(len(expected)):
                self.assertEqual(expected[i][0], result[i][0])
                self.assertTrue(math.isclose(expected[i][1], result[i][1], rel_tol=0.05))

class TestParseKey(unittest.TestCase):
    def testPassingNonsenseReturnsNone(self):
        self.assertIsNone(notes.parseKey("J# Maj"))
        self.assertIsNone(notes.parseKey("Cb Cro"))
        self.assertIsNone(notes.parseKey("G- Maj"))
    def testSimpleKeys(self):
        self.assertEqual((2, 1, "maj", -1), notes.parseKey("C#"))
        self.assertEqual((3, -1, "maj", -1), notes.parseKey("Db"))
        self.assertEqual((0, 0, "min", 0), notes.parseKey("a"))
    def testExplicitModifier(self):
        self.assertEqual((3, 0, "min", -1), notes.parseKey("D minor"))
        self.assertEqual((0, 0, "min", 0), notes.parseKey("A min"))
        self.assertEqual((4, 0, "maj", -1), notes.parseKey("e maj"))
        self.assertEqual((5, 0, "maj", -1), notes.parseKey("f major"))
    def testExplicitOctave(self):
        self.assertEqual(-2, notes.parseKey("F_3")[3])
        self.assertEqual(1, notes.parseKey("A_5")[3])

class Accumulator:
    s = ""
    def add(self, t):
        self.s += t + "\n"
    def get(self):
        return self.s
    def reset(self):
        self.s = ""


class TestPrintScale(unittest.TestCase):
    def testPrintScale(self):
        testDataMajor = """name Major scale 
-------------
note\t   0.00 Hz
othernote\t   1.00 Hz
"""
        testDataMinor = """name minor scale 
-------------
note\t   0.00 Hz
othernote\t   1.00 Hz
"""
        acc = Accumulator()
        
        with unittest.mock.patch.multiple(
            'notes',
            displayName = MagicMock(
                return_value="name"
                ),
            getScale = MagicMock(
                return_value=[("note", 0.0), ("othernote", 1.0)]
                ),
            parseKey = MagicMock(
                side_effect=[(0, 0, "maj", 0), (0, 0, "min", 0)]
                )
            ) as values:
                ret = notes.printScale("")
                self.assertEqual(testDataMajor, ret)
                ret = notes.printScale("")
                self.assertEqual(testDataMinor, ret)

class TestPlayScale(unittest.TestCase):
    def testPlayDefault(self):
        testData = [(None,0),(None,1),(None,2),(None,3),(None,4)]
        ctx = Mock()
        player = MagicMock()
        player.playNote = MagicMock()
        ctx.__enter__ = Mock(return_value = player)
        ctx.__exit__ = Mock()
        with unittest.mock.patch('notes.getScale', return_value=testData):
            notes.playScale(ctx, "C")
        calls = player.playNote.call_args_list
        player.playNote.assert_called()
        self.assertEqual(5, len(calls))
        for i in range(len(calls)):
            self.assertEqual(i, calls[i][0][0])
            

class TestMorse(unittest.TestCase):
    def testQueueDot(self):
        f = 440
        s = 0.1
        player = Mock()
        player.queueNote = Mock()
        coder = morse.MorseCoder(player, f, s)
        coder.queueDot()
        player.queueNote.assert_called_once_with(f, s)
    def testQueueDash(self):
        f = 440
        s = 0.1
        player = Mock()
        player.queueNote = Mock()
        coder = morse.MorseCoder(player, f, s)
        coder.queueDash()
        player.queueNote.assert_called_once_with(f, s * 3)
        
if __name__ == "__main__":
    unittest.main()
