import re
import pyaudio
from sine_wave import NotePlayerContext, decibel

# regular expression used to parse key names
# it matches a single upper or lower case A-G, then an optional
# string of sharps or flats, then an optional underscore and octave number
# then an optional chunk of whitespace, then optionally either the word "maj"
# or "min" (to specify the scale. It does not check for the end of the
# string, so the following all match:
# * C#
# * C# Maj
# * C# Major
# * C# Minor
# * C# Majnificent Seven
regex = re.compile("([A-G])(#+|b+)?(?:_(\d+))?\s*(maj|min)?(?:or)?$", re.I)

#Definition of A (A_4 in scientific pitch notation) is 440 Hz
A = 440

#The twelve notes of western music, in ascending order
note   = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
#The staff positions associated with the enmarmonic spellings used above
staff_pos = [0, 0, 1, 2, 2, 3, 3, 4, 5, 5, 6, 6]

#Semitone offsets from the tonic for various scales:
#The major scale
maj       = [0, 2, 4, 5, 7, 9, 11, 12]
#The natural minor scale
nat_min   = [0, 2, 3, 5, 7, 8, 10, 12]
#The chromatic scale (all of the notes) -- currently unusued in this program
chromatic = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
#The harmonic minor -- currently unusued in this program
h_min   = [0, 2, 3, 5, 7, 8, 11, 12]

#The key of A minor
A_min   = ["A", "B", "C", "D", "E", "F", "G"]

# displayName -- take a staff position and modifier and return
# the ordinary human name for that note
#
# parameters:
# pos: the staff position of the note; A=0, B=1, etc.
# modifier: the number of semitones above or below the
# natural the given note is
#
# WARNING: this function **WILL** return silly things if passed a silly input
# eg, giving it (3, 11) will output D###########, not Db as you might expect
#
####
def displayName(pos, modifier):
    # As mentioned below, A minor (natural) is the notes A-G in order, so
    # we use it to look up the name of the note at that staff positon
    # and then add the correct number of sharps/flats
    if modifier < 0:
        return A_min[pos] + (-modifier) * "b"
    if modifier > 0:
        return A_min[pos] + (modifier) * "#"
    else:
        return A_min[pos]

# enharmonise -- writes a given semitone as a variant of the given
# staff position
#
# parameters:
# semitone: the number of semitones above A440 this pitch is
# pos: the staff position to write it in terms of; 0 is A, 1 is B, etc.
#
####
def enharmonise(semitone, pos):
    #make sure the staff position and semitone number are in canonical range
    pos = pos % 7
    semitone = semitone % 12

    #It happens that the scale of A minor (natural) has all of the "white keys"
    #And begins with A, meaning we can use the natural minor to convert
    #a staff position into a number of semitones
    target = nat_min[pos]

    #The difference between the actual semitone and the semitone associated
    #with the staff position natural is the number of sharps/flats we need
    diff = semitone - target

    #Twelve sharps (or flats) will give the same note as was started with,
    #so we can take the difference modulo 12. Mathematically, there are
    #a few different ways to do this, but we take the tone difference to
    #be between -6 and 5 (inclusive) for notational convenience.
    #
    #In real music, we almost never see a double sharp/flat and a treble
    #sharp/flat is a thing of myth and legend.
    diff = diff % 12

    if  diff > 5:
        diff = diff - 12

    
    return displayName(pos, diff)

def getScale(basePos, modifier, tag, octave):
    #calculate how many semitones above A440 the tonic note is
    baseTone = nat_min[basePos] + modifier

    #set the scale to use -- major or minor
    if tag == "maj":
        scale = maj
    elif tag == "min":
        scale = nat_min
    else:
        raise ValueError("Bad tag passed to getScale")

    notes = []
    for j in range(8):
        #get the number of semitones above the tonic the current note is
        #this is the same regardless of what the tonic actually is
        #it depends only on the type of scale
        semitoneOffset = scale[j]
        #calculate the actual frequency of the note, using A440 as a base
        #to move up one semitone, we multiply the frequency by the twelfth
        #root of two. This ensures that after we have moved up twelve
        #semitones (one octave) the frequency has exactly doubled
        freq = A * 2 ** ( (baseTone + semitoneOffset) /12 + octave)

        #append the correct enharmonic spelling (calculated from
        #the current staff position and semitone) along with
        #the calculated frequency
        notes.append((enharmonise(baseTone + semitoneOffset, basePos + j), freq))
    return notes

# parseKey -- parse the name of a musical key into 
# parameters:
# key: name of a musical key
# examples of accepted forms:
# f#
# Gb
# A min
# C_2 major
#
# if parsed, returns a quadruplet (pos, modifier, tag, octave) where:
# * pos is the generic staff position; 0 for A,  1 for B, etc.
# * modifier is the number of semitones above or below the natural
# of that staff position that the given key is; +1 is sharp, -1 is flat
# * tag is the kind of scale; currently returns "min" for a minor scale
# or "maj" for a major scale
# * octave is the octave we are in; our internal octaves start from A,
# and A440 is in octave 0. A220 is octave -1, A880 octave 1, etc.
#
# if the name could not be parsed, returns None
#
####
def parseKey(key):
    # We begin by matching the input against a regular expression we
    # defined at the top of the file; if it does not match, return None
    result = regex.match(key)
    if result is None:
        return None

    # Next, we fetch the match groups

    #the letter name of the note
    note = result.group(1)
    #the accidentals (if any) after it
    accidentals = result.group(2)
    #the octave number (if any) specified
    octave = result.group(3)
    #and the scale specifier (if any)
    specifier = result.group(4)

    #here we make sure that the accidentals are a lower-case string
    #this means we don't have to check as much later, we can just assume
    if accidentals is None:
        accidentals = ""
    accidentals = accidentals.lower()

    #by default, we use a minor scale if the note is lower case
    #and a major key if it is upper case, as is convention
    if note.isupper():
        tag = "maj"
    else:
        tag = "min"

    #if a scale was explicitly specified, use that instead
    #the regex assures that the value in specifier must be a valid
    #scale name
    if specifier is not None:
        tag = specifier.lower()

    #we define the staff position as being A=0, B=1, etc
    pos = A_min.index(note.upper())

    # We default the octave to 0
    # Our octave 0 is A_4 - A_5
    if octave is None:
        octave = 0
    else:
        #if an octave was specified, we use that, subtracting 4
        octave = int(octave)
        octave = octave - 4
    # standard octaves start with C, but ours start at A. To fix
    # this, we subtract one from the octave number if we're at or
    # after C. This ensures that if the user inputs C_4 they get
    # a lower pitch than if they input A_4
    if pos >= 2:
        octave = octave - 1

    # The number of sharps or flats after the note gives us the distance
    # from the natural in semitones
    modifier = len(accidentals)
    # and if it is flat, that is a negative number of semitones, to
    # indcate that it is below the natural
    if modifier > 0 and accidentals[0] == "b":
        modifier = - modifier

    return (pos, modifier, tag, octave)


# printScale -- print out a musical scale given a key
# parameters:
# key: name of a musical key, in a form accepted by parseKey (eg G# min)
# mode: optional mode, overrides the setting minor/major parsed from
# the key name
#
####
def printScale(key, mode = None):
    ret = "";
    result = parseKey(key)
    if mode is None:
        mode = result[2]
    name = displayName(result[0], result[1])
    scale = getScale(result[0], result[1], mode, result[3])
    if mode == "maj":
        ret += "%s Major scale \n" % name
    elif mode == "min":
        ret += "%s minor scale \n" % name
    ret += "-------------\n"
    for k in scale:
        ret += "%s\t%7.2f Hz\n" % k
    return ret

# playScale -- plays the notes of a scale through the default audio sink
# parameters:
# key: name of a musical key, in a form accepted by parseKey (eg G# min)
# mode: optional mode, overrides the setting minor/major parsed from
# the key name
#
####
def playScale(ctx, key, mode = None, volume = None):
    if volume is None:
        volume = decibel(-10)
    result = parseKey(key)
    if mode is None:
        mode = result[2]
    scale = getScale(result[0], result[1], mode, result[3])
    with ctx as player:
        for note in scale:
            player.playNote(note[1], 0.25, volume)


# mainLoop -- prompt for a scale name, then print and play it
# parameters:
# ctx: pyaudio context to use to play notes
#
####
def mainLoop(pya):
    try:
        while True:
            while True:
                name = input("Enter scale to print (q to quit): ")
                if name.lower()=="q":
                    raise RuntimeError()
                result = parseKey(name)
                if result is not None:
                    break
                print("Bad scale name, try again")
            print("")
            print(printScale(name))
            ctx = NotePlayerContext(pya, 44100)
            playScale(ctx, name)
    except RuntimeError:
        pass

if __name__ == "__main__":
    import pyaudio
    ctx = pyaudio.PyAudio()
    mainLoop(ctx)
    ctx.terminate()
