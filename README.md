# PyNotes [![pipeline status](https://gitlab.com/qxzkjp/pynotes/badges/master/pipeline.svg)](https://gitlab.com/qxzkjp/pynotes/commits/master) [![coverage report](https://gitlab.com/qxzkjp/pynotes/badges/master/coverage.svg)](https://gitlab.com/qxzkjp/pynotes/commits/master)

Plays a scale (and prints the notes) of a given key.

## Installation
the script requires numpy and pyaudio (for playing the notes). To install them, run

```shell
pip install numpy
pip install pyaudio
```

If you're on Windows, you might need to install the pre-built wheel file for pyaudio, which can be found [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio)

To install it the wheel file, run
```shell
cd path/to/file
pip install <filename>.whl
```

## Usage

Run `notes.py`, and follw the prompts

* Input a note in upper case to get the major scale
  * C#
  * A
  * Gb
* Input a note in lower case to get the minor scale
  * d
  * bb
  * f#
* You can also follow a note name with "maj" or "min" to get the respective scales
  * C min
  * G# min
  * F major
  * Eb minor
* By default, the notes are played in the fourth octave of scientific pitch. To specify a different octave number, place an underscore and an octave number directly after the note name
  * Eb_1
  * a_6
  * B#_2
 